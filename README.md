# Que es languages
Languages es el repositorio de orchestra donde se almacenan las cadenas de texto traducidas del proyecto. Está sincronizado con la plataforma POEDitor para una mejor gestión de las traducciones.

# Como instalo languages
Para utilizar languages en un proyecto se recomienda instalarlo como submodule:

git submodule add git@bitbucket.org:mirubee/languages.git

Si ya descargaste el proyecto con el submodulo añadido en el repositorio entonces hay que realizar:
git submodule update --init
Con este comando indicaremos a nuestro repositorio local que actualice el submodulo.

Una vez instalado hay que hacer pull en la carpeta donde hayas instalado las traducciones, y después hacer commit del proyecto general para incluir los nuevos cambios.

# Como subir las traducciones del traductor
Cuando se crean los términos y se realizan las traducciones necesitamos subir los cambios que genera al repositorio, para ello se han creado dos hooks que tras ejecutarlos suben automáticamente las traducciones del traductor al repositorio.

El link a ejecutar para subir las traducciones de inglés es el siguiente:
https://api.poeditor.com/webhooks/05d1c6b220

Y el link para castellano:
https://api.poeditor.com/webhooks/2389c171f6